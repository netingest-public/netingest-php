# Changelog

## [0.0.1][0.0.1] - 2023-03-09

### Added

- Add Sdk PHP class

## [0.0.2][0.0.2] - 2023-03-11

### Fixed

- Fix namespacing for PHP classes

## [0.0.3][0.0.3] - 2023-03-28

### Added

- Added HTML templates to handle ingest API statuses

## [0.0.4][0.0.4] - 2023-03-28

### Added

- Added simple captcha

## [0.0.5][0.0.5] - 2023-04-06

### Fixed

- Fixed the captcha verification

## [0.0.6][0.0.6] - 2023-04-17

### Added

- Added regex to ignore website assets

## [0.0.7][0.0.7] - 2023-04-20

### Added

- Added reCaptcha verification

## [0.0.8][0.0.8] - 2023-04-24

### Added

- Added error handling when the DSN is wrong

## [0.0.9][0.0.9] - 2023-05-17

### Updated

- Support for PHP 7.2.0

## [0.0.10][0.0.10] - 2023-07-17

### Updated

- Update composer authors

## [0.0.11][0.0.11] - 2023-09-18

### Updated

- Update composer authors  