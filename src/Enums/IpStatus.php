<?php

namespace NetIngest\Enums;

class IpStatus
{
    public const UNKNOWN = 'unknown';
    public const ALLOW = 'allow';
    public const DENY = 'deny';
    public const THROTTLED = 'throttled';
    public const CAPTCHA = 'captcha';
}
