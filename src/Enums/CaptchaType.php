<?php

namespace NetIngest\Enums;

/**
 * CaptchaType
 */
class CaptchaType
{
    public const RECAPTCHA = 'recaptcha';

    public const CLASSIC = 'classic';
}
