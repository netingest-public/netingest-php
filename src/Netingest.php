<?php

namespace NetIngest;

use NetIngest\SDK\SDK;

class Netingest
{
    private static ?Netingest $instance = null;

    protected static SDK $sdk;

    private function __construct()
    {
    }

    public static function getInstance(): Netingest
    {
        if (self::$instance == null) {
            self::$instance = new Netingest();
        }

        return self::$instance;
    }

    /**
     * Create new SDK instance
     *
     * @param string $dsn
     * @return SDK
     */
    public static function sdk(string $dsn): SDK
    {
        $instance = self::getInstance();
        if (empty($instance::$sdk) || defined('PHP_UNIT_TEST')) {
            $instance::$sdk = new SDK($dsn);
        }
        return $instance::$sdk;
    }
}
