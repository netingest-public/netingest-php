<?php

namespace NetIngest\SDK;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;

class SDK
{
    /**
     * The url of the Netingest API
     *
     * @var string $dsn
     */
    protected string $dsn;

    /**
     * Force a hostname to the api, this disables the auto-detection.
     * Do not use this, unless you know what you are doing.
     *
     * @var string
     */
    protected string $hostName = '';

    /**
     * Whether the script should just return the raw json response from the Netingest API
     * Only use this if you want to handle the response yourself
     *
     * @var bool
     */
    protected bool $returnRawApiResponse = false;

    /**
     * Enable/Disable debugging
     *
     * @var bool
     */
    protected bool $debug = false;

    /**
     * Debug log path (empty used php_error_log)
     *
     * @var string
     */
    protected string $debugLogPath = '';

    /**
     * Whether ssl verification is enabled/disabled on the Curl request
     * Disabling this will speed up the request, but it's less secure
     *
     * @var bool
     */
    protected bool $sslVerification = false;

    /**
     * Blacklisted headers, these headers will not be sent to the Netingest API
     * You should never send sensitive data to the Netingest API
     *
     * @var array <string>
     */
    protected array $blacklistedHeaders = [
        'Cookie',
        'User-Agent',
        'Authorization'
    ];

    /**
     * @var array<string>
     */
    protected array $excludedFileExtensions = [];

    /**
     * Maximum curl execution time
     *
     * @var int
     */
    protected int $defaultCurlOptTimeout = 2;

    /**
     * Maximum curl connection time
     *
     * @var int
     */
    protected int $defaultCurlOptConnectTimeout = 0;

    /**
     * Mocked API response for unit tests
     *
     * @var array<string>
     */
    protected array $mockedApiResponses = [];

    public function __construct(string $dsn)
    {
        $this->dsn = $dsn;
    }

    /**
     * Force the hostname to the api, this disables the auto-detection.
     * Do not use this, unless you know what you are doing.
     *
     * @param string $hostName
     * @return void
     */
    public function setHostName(string $hostName): void
    {
        $this->hostName = $hostName;
    }

    /**
     * Get the hostname of this request
     *
     * @return string
     */
    public function getHostName(): string
    {
        if (!empty($this->hostName)) {
            return $this->hostName;
        }
        return $_SERVER['HTTP_HOST'] ?? '';
    }

    /**
     * Set a mocked API response for unit tests
     *
     * @param array<string> $mockedApiResponses
     * @return void
     */
    public function setMockedApiResponses(array $mockedApiResponses): void
    {
        $this->mockedApiResponses = $mockedApiResponses;
    }

    /**
     * Get the mocked API response for unit tests
     *
     * @return string
     */
    public function getMockedApiResponse(): string
    {
        //Get the first mocked API response from the array
        return (string)array_shift($this->mockedApiResponses);
    }

    /**
     * Do we have any mocked API responses?
     *
     * @return bool
     */
    public function hasMockedApiResponses(): bool
    {
        return !empty($this->mockedApiResponses);
    }

    /**
     * Set The maximum curl execution time
     *
     * @param int $timeout
     * @return void
     */
    public function setDefaultCurlOptTimeout(int $timeout): void
    {
        $this->defaultCurlOptTimeout = $timeout;
    }

    /**
     * Set The maximum curl connection time
     *
     * @param int $timeout
     * @return void
     */
    public function setDefaultCurlOptConnectTimeout(int $timeout): void
    {
        $this->defaultCurlOptConnectTimeout = $timeout;
    }

    /**
     * Enable/Disable debugging
     *
     * @param bool $debug
     * @return void
     */
    public function setDebug(bool $debug): void
    {
        $this->debug = $debug;
    }

    /**
     * Where should we log to?
     *
     * @param string $debugLogPath
     * @return void
     */
    public function setDebugLogPath(string $debugLogPath): void
    {
        $this->debugLogPath = $debugLogPath;
    }

    /**
     * Whether ssl verification is enabled/disabled on the Curl request
     *
     * Disabling this will speed up the request, but it's less secure
     *
     * @param bool $sslVerification
     * @return void
     */
    public function setSSLVerification(bool $sslVerification): void
    {
        $this->sslVerification = $sslVerification;
    }

    /**
     * Return the raw API response, so you can handle it yourself
     *
     * @param bool $returnRawApiResponse
     * @return void
     */
    public function setReturnRawApiResponse(bool $returnRawApiResponse): void
    {
        $this->returnRawApiResponse = $returnRawApiResponse;
    }

    /**
     * Is Curl SSL verification disabled?
     *
     * @return bool
     */
    public function isSslVerificationEnabled(): bool
    {
        return $this->sslVerification;
    }

    /**
     * Do we need to return the raw API response?
     *
     * @return bool
     */
    public function isReturnRawApiResponse(): bool
    {
        return $this->returnRawApiResponse;
    }

    /**
     * Get the API DSN
     *
     * @return string
     */
    public function getDsn(): string
    {
        return $this->dsn;
    }

    /**
     * Get Maximum curl execution time
     *
     * @return int
     */
    public function getMaxCurlExecutionTime(): int
    {
        return $this->defaultCurlOptTimeout;
    }

    /**
     * Get Maximum curl connection time
     *
     * @return int
     */
    public function getMaxCurlConnectionTime(): int
    {
        return $this->defaultCurlOptConnectTimeout;
    }

    /**
     * Add blacklisted header, this header will not be sent to the Netingest API
     * You should never send sensitive data to the Netingest API
     *
     * @param array<string> $headers
     * @return void
     */
    public function setBlacklistedHeaders(array $headers): void
    {
        $this->blacklistedHeaders = $headers;
    }

    /**
     * Add file extensions that should be skipped
     *
     * @param array<string> $extensions
     * @return void
     */
    public function setExcludedFileExtensions(array $extensions): void
    {
        $this->excludedFileExtensions = $extensions;
    }

    /**
     * Get blacklisted headers
     *
     * @return array <string>
     */
    public function getBlacklistedHeaders(): array
    {
        return $this->blacklistedHeaders;
    }

    /**
     * @return string
     */
    public function init(): string
    {
        $this->initLog();

        // If there is no REQUEST_URI, we were probably called from the CLI
        if (empty($_SERVER['REQUEST_URI'])) {
            $this->log('Skipped by empty REQUEST_URI');
            return '';
        }

        // Skip file extensions, like website assets
        if ($this->skipByFileExtension($_SERVER['REQUEST_URI'])) {
            $this->log('Skipped by file extension');
            return '';
        }

        try {
            $html = Router::route($this);
            if (!empty($html)) {
                if (!defined('PHP_UNIT_TEST')) {
                    echo $html;
                    exit;
                }
            }
            return $html;
        } catch (NetingestRawResponseException $e) {
            return $e->getMessage();
        } catch (NetingestException $e) {
            $this->log($e->getMessage());
            return '';
        }
    }

    /**
     * Collect the IP address of the request
     *
     * @return string
     */
    public function getIpAddress(): string
    {
        // Get real visitor IP behind Load Balancers
        if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            // Get First ip from proxy chain
            if (strstr($_SERVER["HTTP_X_FORWARDED_FOR"], ',')) {
                $ips = explode(',', $_SERVER["HTTP_X_FORWARDED_FOR"]);
                return trim($ips[0]);
            } else {
                return $_SERVER["HTTP_X_FORWARDED_FOR"];
            }
        }
        return $_SERVER['REMOTE_ADDR'] ?? '127.0.0.1';
    }

    /**
     * Skip the verification by file extension
     *
     * @param string $request_uri
     * @return bool
     */
    protected function skipByFileExtension(string $request_uri): bool
    {
        // Skip if no file extensions are excluded
        if (empty($this->excludedFileExtensions)) {
            return false;
        }
        $regex = '/\.(' . implode('|', $this->excludedFileExtensions) . ')$/';

        if (preg_match($regex, $request_uri) === 0) {
            return false;
        }
        return true;
    }

    /**
     * Get all headers (works with all HTTP servers)
     *
     * @return array<string, string>
     */
    public function getAllHttpHeaders(): array
    {
        $headers = [];
        foreach ($_SERVER as $name => $value) {
            if (substr($name, 0, 5) === 'HTTP_') {
                $header_name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))));
                if (in_array($header_name, $this->blacklistedHeaders)) {
                    continue;
                }
                $headers[$header_name] = $value;
            }
        }
        return $headers;
    }

    /**
     * Log debug information
     *
     * @param string $message
     * @return void
     */
    public function log(string $message): void
    {
        if ($this->debug) {
            if (empty($this->debugLogPath)) {
                error_log($message);
            } else {
                file_put_contents($this->debugLogPath, date('Y-m-d H:i:s') . ' ' . $message . "\n", FILE_APPEND);
            }
        }
    }

    /**
     * Log request information
     *
     * @return void
     */
    protected function initLog(): void
    {
        $this->log('--------------------' . date('Y-m-d H:i:s') . '--------------------');
        $this->log('REQUEST_URI: ' . ($_SERVER['REQUEST_URI'] ?? ''));
        $this->log('HTTP_USER_AGENT: ' . ($_SERVER['HTTP_USER_AGENT'] ?? ''));
        $this->log('HTTP_REFERER: ' . ($_SERVER['HTTP_REFERER'] ?? ''));
        $this->log('HTTP_X_FORWARDED_FOR: ' . ($_SERVER['HTTP_X_FORWARDED_FOR'] ?? ''));
        $this->log('HTTP_HOST: ' . ($_SERVER['HTTP_HOST'] ?? ''));
        $this->log('REMOTE_ADDR: ' . ($_SERVER['REMOTE_ADDR'] ?? ''));
        $this->log('REMOTE_IP (detected): ' . $this->getIpAddress());
        $this->log('REQUEST_METHOD: ' . ($_SERVER['REQUEST_METHOD'] ?? ''));
        //Log all $_POST variables, one log per line
        foreach ($_POST as $key => $value) {
            if (is_string($value)) {
                $this->log('POST: ' . $key . ' => ' . $value);
            }
        }
    }
}
