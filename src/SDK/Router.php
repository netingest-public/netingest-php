<?php

namespace NetIngest\SDK;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\Actions\Captcha;
use NetIngest\SDK\Actions\Recaptcha;
use NetIngest\SDK\RequestHandlers\Access\AccessHandler;

class Router
{
    /**
     * Manage which requests we need to send to the Netingest API
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function route(SDK $sdk): string
    {
        if (isset($_POST['ni_action'])) {
            if ($_POST['ni_action'] == 'captcha_verify') {
                $sdk->log('Routed Captcha verification request');
                return static::captchaVerify($sdk);
            } elseif ($_POST['ni_action'] == 'recaptcha_verify') {
                $sdk->log('Routed Recaptcha verification request');
                return static::recaptchaVerify($sdk);
            }
            throw new NetingestException('Invalid action ' . $_POST['ni_action']);
        } else {
            $sdk->log('Routed default request');
            return static::default($sdk);
        }
    }

    /**
     * Response to a captcha verification challenge
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    protected static function captchaVerify(SDK $sdk): string
    {
        return Captcha::verifyChallenge($sdk);
    }

    /**
     * Response to a recaptcha verification challenge
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    protected static function recaptchaVerify(SDK $sdk): string
    {
        return Recaptcha::verifyChallenge($sdk);
    }

    /**
     * Default request
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    protected static function default(SDK $sdk): string
    {
        $data = [
            'host' => $sdk->getHostName(),
            'ip' => $sdk->getIpAddress(),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
            'referrer' => $_SERVER['HTTP_REFERER'] ?? '',
            'request_url' => $_SERVER['REQUEST_URI'],
            'request_method' => $_SERVER['REQUEST_METHOD'],
            'tags' => [],
            'headers' => $sdk->getAllHttpHeaders()
        ];

        return AccessHandler::request($sdk, $data);
    }
}
