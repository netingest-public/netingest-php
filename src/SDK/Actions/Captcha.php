<?php

namespace NetIngest\SDK\Actions;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\RequestHandlers\Captcha\CaptchaVerifyHandler;
use NetIngest\SDK\SDK;

class Captcha
{
    /**
     * Init captcha
     *
     * @param string $secret
     * @param string $imageUrl
     * @return string
     */
    public static function initChallenge(string $secret, string $imageUrl): string
    {
        return static::template($secret, $imageUrl);
    }

    /**
     * Captcha Verification
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function verifyChallenge(SDK $sdk): string
    {
        $fields = [
            'captcha_response' => $_POST['captcha_response'],
            'secret' => $_POST['secret'],
            'host' => $sdk->getHostName(),
            'ip' => $sdk->getIpAddress(),
        ];
        $verified = CaptchaVerifyHandler::request($sdk, $fields) == 1;

        if ($verified) {
            //@codeCoverageIgnoreStart
            $sdk->log('Captcha verification successful');
            if (!defined('PHP_UNIT_TEST')) {
                header("refresh:0");
                exit;
            }
            //@codeCoverageIgnoreEnd
            return '';
        }

        $sdk->log('Captcha verification failed');

        return static::template(
            $_POST['secret'],
            $_POST['image_url'],
            '<div style="color: red; margin-top: .5rem;">The letters you entered are incorrect.</div>'
        );
    }

    /**
     * Template
     *
     * @param string $secret
     * @param string $imageUrl
     * @param string $validationMessage
     * @return string
     */
    protected static function template(string $secret, string $imageUrl, string $validationMessage = ''): string
    {
        return <<<HTML
        <html lang="en">
        <head>
            <title>Captcha verification</title>
        </head>
        <body style="height: 100vh; margin: 0; display: flex; justify-content: center; align-items: center">
        <form id="captcha-form" method="POST">
            <div>
                <?php /** @phpstan-ignore-next-line */ ?>
                <img src="{$imageUrl}" alt="Captcha word" />
            </div>
            <div style="margin-top: 1rem;">
                <label for="captcha-response" style="margin-bottom: .5rem; display: block">Enter the letters:</label>
                <input type="hidden" name="ni_action" value="captcha_verify" />
                <input type="hidden" name="secret" value="{$secret}" />
                <input type="hidden" name="image_url" value="{$imageUrl}" />
                <input type="text" name="captcha_response" id="captcha-response" style="width: 100%;
            border-radius: 0.5rem;
            border-width: 1px;
            border-color: #432cf3;
            background-color: #ffffff;
            padding: 0.5625rem 1rem;
            font-size: 1rem;
            line-height: 1.5rem;" />
                {$validationMessage}
            </div>
            <button type="submit" style="width: 100%;
            color: #ffffff;
            margin-top: 1rem;
            background-color: #432cf3;
            border-style: solid;
            border-width: 1px 1px 1px 1px;
            border-color: #432cf3;
            border-radius: 8px;
            padding: 9px 12px;
            font-size: 16px;
            font-weight: 500;
            line-height: 24px;
            cursor: pointer">Submit</button>
        </form>
        </body>
        </html>
        HTML;
    }
}
