<?php

namespace NetIngest\SDK\Actions;

class Deny
{
    public static function template(): string
    {
        http_response_code(403);
        return <<<HTML
        <html lang="en">
        <head>
            <title>Forbidden</title>
        </head>
        <body style="height: 100vh; margin: 0; display: flex; justify-content: center; align-items: center">
        <div>
            <h1>Forbidden</h1>
            <p>You do not have permission to access this resource.</p>
        </div>
        </body>
        </html>
        HTML;
    }
}
