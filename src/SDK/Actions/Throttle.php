<?php

namespace NetIngest\SDK\Actions;

class Throttle
{
    public static function template(): string
    {
        http_response_code(429);
        return <<<HTML
        <html lang="en">
        <head>
            <title>Too many requests</title>
        </head>
        <body style="height: 100vh; margin: 0; display: flex; justify-content: center; align-items: center">
        <div>
            <h1>Too many requests</h1>
            <p>The number of allowed requests has been reached. Try again soon.</p>
        </div>
        </body>
        </html>
        HTML;
    }
}
