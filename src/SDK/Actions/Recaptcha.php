<?php

namespace NetIngest\SDK\Actions;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\RequestHandlers\Recaptcha\RecaptchaVerifyHandler;
use NetIngest\SDK\SDK;

class Recaptcha
{
    /**
     * Init captcha
     *
     * @param SDK $sdk
     * @param string $recaptcha_site_key
     * @return string
     */
    public static function initChallenge(SDK $sdk, string $recaptcha_site_key): string
    {
        return static::template($sdk, $recaptcha_site_key);
    }

    /**
     * Captcha Verification
     *
     * @param SDK $sdk
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function verifyChallenge(SDK $sdk): string
    {
        $fields = [
            'recaptcha_response' => $_POST['g-recaptcha-response'],
            'hostname' => $sdk->getHostName(),
            'ip' => $sdk->getIpAddress(),
        ];
        $verified = RecaptchaVerifyHandler::request($sdk, $fields) == 1;

        if ($verified) {
            $sdk->log('Recaptcha verification successful');
            //@codeCoverageIgnoreStart
            if (!defined('PHP_UNIT_TEST')) {
                header("refresh:0");
                exit;
            }
            //@codeCoverageIgnoreEnd
            return '';
        }

        $sdk->log('Recaptcha verification failed');

        return static::template(
            $sdk,
            $_POST['recaptcha_site_key'],
            '<div style="color: red; margin-top: .5rem;">Verification Failed, try again.</div>'
        );
    }

    /**
     * This returns the HTML for the recaptcha form, or checks the captcha response
     *
     * @param SDK $sdk
     * @param string $recaptcha_site_key
     * @param string $validation_message
     * @return string
     */
    public static function template(SDK $sdk, string $recaptcha_site_key, string $validation_message = ''): string
    {
        return <<<HTML
        <html lang="en">
        <head>
            <title>reCAPTCHA verification</title>
            <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        </head>
        <body style="height: 100vh; margin: 0; display: flex; justify-content: center; align-items: center">
        <form id="recaptcha-form" method="POST">
            <input type="hidden" name="ni_action" value="recaptcha_verify">
            <input type="hidden" name="recaptcha_site_key" value="{$recaptcha_site_key}">
            <div style="margin-top: 1rem;">
                <div class="g-recaptcha" data-sitekey="{$recaptcha_site_key}"></div>
                {$validation_message}
            </div>
            <button type="submit" style="width: 100%;
                color: #ffffff;
                margin-top: 1rem;
                background-color: #432cf3;
                border-style: solid;
                border-width: 1px 1px 1px 1px;
                border-color: #432cf3;
                border-radius: 8px;
                padding: 9px 12px;
                font-size: 16px;
                font-weight: 500;
                line-height: 24px;
                cursor: pointer">Submit</button>
        </form>
        </body>
        </html>
        HTML;
    }
}
