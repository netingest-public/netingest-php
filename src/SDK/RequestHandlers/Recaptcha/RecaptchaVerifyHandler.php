<?php

namespace NetIngest\SDK\RequestHandlers\Recaptcha;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\RequestHandlers\APIHandler;
use NetIngest\SDK\SDK;

class RecaptchaVerifyHandler
{
    /**
     * @param SDK $sdk
     * @param array<mixed> $data
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function request(SDK $sdk, array $data): string
    {
        $response = APIHandler::request(
            $sdk,
            $sdk->getDsn() . '/recaptcha',
            $data
        );

        return self::processResponse($sdk, $response);
    }

    /**
     * @param SDK $sdk
     * @param string $response
     * @return string
     */
    protected static function processResponse(SDK $sdk, string $response): string
    {
        $json = json_decode($response, true);
        return $json['data']['captcha_verified'];
    }
}
