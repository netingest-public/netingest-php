<?php

namespace NetIngest\SDK\RequestHandlers\Access;

use NetIngest\Enums\IpStatus;
use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\Actions\Captcha;
use NetIngest\SDK\Actions\Deny;
use NetIngest\SDK\Actions\Recaptcha;
use NetIngest\SDK\Actions\Throttle;
use NetIngest\SDK\RequestHandlers\APIHandler;
use NetIngest\SDK\SDK;

class AccessHandler
{
    /**
     * Request ip access status to the api
     *
     * @param SDK $sdk
     * @param array<mixed> $data
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function request(SDK $sdk, array $data): string
    {
        $response = APIHandler::request(
            $sdk,
            $sdk->getDsn() . '/access',
            [
                'data' => json_encode($data)
            ]
        );

        return self::processResponse($sdk, $response);
    }

    /**
     * Process ip access response
     *
     * @param SDK $sdk
     * @param string $response
     * @return string
     * @throws NetingestException
     */
    protected static function processResponse(SDK $sdk, string $response): string
    {
        $json = json_decode($response, true);
        if (!empty($json['code'])) {
            throw new NetingestException('Api returned invalid response: ' . $response);
        }
        if ($json['data']['ip_status'] == IpStatus::ALLOW) {
            $sdk->log('The request has been allowed by the Netingest API');
            return '';
        } elseif ($json['data']['ip_status'] == IpStatus::DENY) {
            $sdk->log('The request has been denied by the Netingest API');
            return Deny::template();
        } elseif ($json['data']['ip_status'] == IpStatus::THROTTLED) {
            $sdk->log('The request has been throttled by the Netingest API');
            return Throttle::template();
        } elseif ($json['data']['ip_status'] == IpStatus::CAPTCHA) {
            if ($json['data']['captcha_data']['captcha_type'] == 'recaptcha') {
                $sdk->log('The request requires recaptcha verification by the Netingest API');
                return Recaptcha::initChallenge(
                    $sdk,
                    $json['data']['captcha_data']['recaptcha_site_key']
                );
            } elseif ($json['data']['captcha_data']['captcha_type'] == 'classic') {
                $sdk->log('The request requires captcha verification by the Netingest API');
                return Captcha::initChallenge(
                    $json['data']['captcha_data']['secret'],
                    $json['data']['captcha_data']['image_url']
                );
            } else {
                throw new NetingestException('Unsupported captcha type: ' . $json['data']['captcha_data']['captcha_type']);
            }
        }
        throw new NetingestException('The response from the Netingest API is not valid JSON string when requesting the access');
    }
}
