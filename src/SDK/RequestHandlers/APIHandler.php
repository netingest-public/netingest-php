<?php

namespace NetIngest\SDK\RequestHandlers;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\SDK;

class APIHandler
{
    /**
     * Request to the Netingest API
     *
     * @param SDK $sdk
     * @param string $url
     * @param array<mixed> $fields
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function request(SDK $sdk, string $url, array $fields = []): string
    {
        $handle = curl_init();
        $curl_settings = [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => $sdk->getMaxCurlExecutionTime(),
            CURLOPT_CONNECTTIMEOUT => $sdk->getMaxCurlExecutionTime(),
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $fields,
        ];
        if (!$sdk->isSslVerificationEnabled()) {
            $curl_settings[CURLOPT_SSL_VERIFYPEER] = false;
            $curl_settings[CURLOPT_SSL_VERIFYHOST] = false;
        }
        curl_setopt_array($handle, $curl_settings);
        if (!empty($sdk->hasMockedApiResponses())) {
            $response = $sdk->getMockedApiResponse();
        } else {
            //@codeCoverageIgnoreStart
            $response = curl_exec($handle);
            //@codeCoverageIgnoreEnd
        }

        if ($response === false) {
            $error = curl_error($handle);
            curl_close($handle);
            //throw exception
            throw new NetingestException('An error occurred when requesting the Netingest API: ' . $error);
        }
        curl_close($handle);

        if ($sdk->isReturnRawApiResponse()) {
            throw new NetingestRawResponseException((string)$response);
        }

        //Return the raw response
        return (string)$response;
    }
}
