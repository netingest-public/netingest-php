<?php

namespace NetIngest\SDK\RequestHandlers\Captcha;

use NetIngest\Exception\NetingestException;
use NetIngest\Exception\NetingestRawResponseException;
use NetIngest\SDK\RequestHandlers\APIHandler;
use NetIngest\SDK\SDK;

class CaptchaVerifyHandler
{
    /**
     * Send Captcha Verification Request
     *
     * @param SDK $sdk
     * @param array<mixed> $data
     * @return string
     * @throws NetingestException
     * @throws NetingestRawResponseException
     */
    public static function request(SDK $sdk, array $data): string
    {
        $response = APIHandler::request(
            $sdk,
            $sdk->getDsn() . '/captcha',
            $data
        );

        return self::processResponse($response);
    }

    /**
     * Process response of verification request
     *
     * @param string $response
     * @return string
     */
    protected static function processResponse(string $response): string
    {
        $json = json_decode($response, true);
        return $json['data']['captcha_verified'];
    }
}
