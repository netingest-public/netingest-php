# Installation

Installation on your project

```bash
composer require netingest/netingest
```

# Normal Usage

```php
use NetIngest\NetIngest;

// HTTP is much faster than HTTPS, but insecure. Only use HTTP if you know what you are doing.
$netingest = Netingest::sdk('https://dns.url');
$netingest->init();
```

# Extra options

```php
use NetIngest\NetIngest;

$netingest = Netingest::sdk('https://dns.url');
// OPTIONAL: Disable SSL verification (this speeds up the connection to Netingest 
$netingest->setSSLVerification(false); // Default: false

// OPTIONAL: Set the timeouts when connecting to Netingest
$netingest->setDefaultCurlOptTimeout(2);
$netingest->setDefaultCurlOptConnectTimeout(0);

// OPTIONAL: Debugging
$netingest->setDebug(true); // Default: false
$netingest->setDebugLogPath('/tmp/netingest.log'); // Default: use the PHP error log

// OPTIONAL: Set what headers we don't want to forward to Netingest.
// By default we don't forward the Authorization and Cookie headers.
$netingest->setBlacklistedHeaders(['Authorization','Cookie']); // Default: ['Authorization','Cookie']

$netingest->init();
```

# Custom Integration

Normally Netingest takes care of blocking, throttling or showing captchas on your website.
However, if you want to use Netingest with an API, or customize its integration, you can do it like this:

```php
use NetIngest\NetIngest;
$netingest = Netingest::sdk('https://dns.url');
$netingest->setReturnRawApiResponse(true);
$response=$netingest->init();
// Code to handle the response
```