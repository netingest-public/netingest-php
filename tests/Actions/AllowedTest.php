<?php

namespace NetIngest\Tests\Actions;

use NetIngest\Netingest;
use PHPUnit\Framework\TestCase;

final class AllowedTest extends TestCase
{
    public function testAccessAllowed(): void
    {
        $_SERVER['REMOTE_ADDR'] = '1.1.1.1';
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $mockedResponses = ['{"data":{"ip_status":"allow"}}'];

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponses);
        $response = $netingest->init();

        $this->assertEmpty($response);

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponses);
        $netingest->setReturnRawApiResponse(true);
        $response = $netingest->init();

        $this->assertEquals($mockedResponses[0], $response);
    }
}
