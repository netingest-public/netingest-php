<?php

namespace NetIngest\Tests\Actions;

use NetIngest\Netingest;
use PHPUnit\Framework\TestCase;

final class CaptchaTest extends TestCase
{
    public function testCaptchaJson(): void
    {
        $_SERVER['REMOTE_ADDR'] = '1.1.1.1';
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        unset($_POST['captcha_response']);
        $mockedResponses = [
            '{"data":{"ip_status":"captcha","captcha_data":{"captcha_type":"classic","image_url":"http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis","secret":"5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis"}}}',
            '{"data":{"captcha_verified":"1"}}',
            '{"data":{"captcha_verified":"0"}}'
        ];

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponses);
        $netingest->setReturnRawApiResponse(true);

        // First request
        $response = $netingest->init();
        $this->assertEquals($mockedResponses[0], $response);

        // Second Request processes the captcha code
        $_POST['ni_action'] = 'captcha_verify';
        $_POST['captcha_response'] = '123456';
        $_POST['secret'] = '5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $_POST['image_url'] = 'http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $response = $netingest->init();
        $this->assertEquals($mockedResponses[1], $response);

        // Third request failure
        $_POST['ni_action'] = 'captcha_verify';
        $_POST['captcha_response'] = '123456';
        $_POST['secret'] = '5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $_POST['image_url'] = 'http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $response = $netingest->init();
        $this->assertEquals($mockedResponses[2], $response);
    }

    public function testCaptchaHtml(): void
    {
        $_SERVER['REMOTE_ADDR'] = '1.1.1.1';
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        unset($_POST['captcha_response']);
        $mockedResponses = [
            '{"data":{"ip_status":"captcha","captcha_data":{"captcha_type":"classic","image_url":"http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis","secret":"5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis"}}}',
            '{"data":{"captcha_verified":"1"}}',
            '{"data":{"captcha_verified":"0"}}'
        ];

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponses);

        // First request
        $response = $netingest->init();
        $this->assertStringContainsString('<title>Captcha verification</title>', $response);

        // Second Request processes the captcha code
        $_POST['ni_action'] = 'captcha_verify';
        $_POST['captcha_response'] = '123456';
        $_POST['secret'] = '5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $_POST['image_url'] = 'http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $response = $netingest->init();
        $this->assertEmpty($response);

        // Third request failure
        $_POST['ni_action'] = 'captcha_verify';
        $_POST['captcha_response'] = '123456';
        $_POST['secret'] = '5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $_POST['image_url'] = 'http://captcha.netingest.com/?secret=5tU4IyrIK05YYdH9HYYETq16Vmbz55h2vI0OMuLscMdgVweGVUI0QWGtk4m1YTis';
        $response = $netingest->init();
        $this->assertStringContainsString('The letters you entered are incorrect.', $response);
    }
}
