<?php

namespace NetIngest\Tests\Actions;

use NetIngest\Netingest;
use PHPUnit\Framework\TestCase;

final class DeniedTest extends TestCase
{
    public function testAccessDenied(): void
    {
        $_SERVER['REMOTE_ADDR'] = '1.1.1.1';
        $_SERVER['REQUEST_URI'] = '/';
        $_SERVER['REQUEST_METHOD'] = 'GET';
        $mockedResponse = ['{"data":{"ip_status":"deny"}}'];

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponse);
        $response = $netingest->init();

        $this->assertStringContainsString('<title>Forbidden</title>', $response);

        $netingest = Netingest::sdk('http://localhost:8001');
        $netingest->setMockedApiResponses($mockedResponse);
        $netingest->setReturnRawApiResponse(true);
        $response = $netingest->init();

        $this->assertEquals($mockedResponse[0], $response);
    }
}
